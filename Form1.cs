﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MuslukHavuzApp
{
    public partial class Form1 : Form
    {
        public const string V = "C:\\Users\\HP\\Desktop\\samet foto\\1kf.jpg";

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            numberOfNodes = -1;
        }

        public int x, y;
        public int numberOfNodes=-1;
        private void btnDugumSayisiOnayla_Click(object sender, EventArgs e)
        {
            txtMuslukSayisi.Enabled = false;
            numberOfNodes =Convert.ToInt32(txtMuslukSayisi.Text);
            txtMuslukSayisi.Enabled = true;
        }
        private void txtMuslukSayisi_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if( numberOfNodes == -1)
            {
                MessageBox.Show("Düğümleri yerleştirmeden önce düğüm sayısını girmelisiniz");
            }
            else if (numberOfNodes != 0)
            {
                txtMuslukSayisi.Text = e.X.ToString() + "-" + e.Y.ToString();
                Point nodpoint = new Point(e.X, e.Y);
                txtMuslukSayisi.Text = e.X.ToString() + " - " + e.Y.ToString();
                createNewNode(nodpoint);
                numberOfNodes--;
            }
            else
            {
                MessageBox.Show("daha fazla düğüm oluşturamazsınız");
            }
        }

       

        void createNewNode(Point nodePoint)
        {
            Button nodea = new Button();
            nodea.Location = nodePoint;
            this.Controls.Add(nodea);
        }
        
         
    }

     

}
