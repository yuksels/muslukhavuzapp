﻿namespace MuslukHavuzApp
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMuslukSayisi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDugumSayisiOnayla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtMuslukSayisi
            // 
            this.txtMuslukSayisi.Location = new System.Drawing.Point(118, 10);
            this.txtMuslukSayisi.Name = "txtMuslukSayisi";
            this.txtMuslukSayisi.Size = new System.Drawing.Size(76, 20);
            this.txtMuslukSayisi.TabIndex = 0;
            this.txtMuslukSayisi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMuslukSayisi_KeyPress);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Musluk Sayısı :";
            // 
            // btnDugumSayisiOnayla
            // 
            this.btnDugumSayisiOnayla.Location = new System.Drawing.Point(211, 8);
            this.btnDugumSayisiOnayla.Name = "btnDugumSayisiOnayla";
            this.btnDugumSayisiOnayla.Size = new System.Drawing.Size(142, 40);
            this.btnDugumSayisiOnayla.TabIndex = 2;
            this.btnDugumSayisiOnayla.Text = "Düğüm Sayısı Onayla";
            this.btnDugumSayisiOnayla.UseVisualStyleBackColor = true;
            this.btnDugumSayisiOnayla.Click += new System.EventHandler(this.btnDugumSayisiOnayla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnDugumSayisiOnayla);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMuslukSayisi);
            this.Name = "Form1";
            this.Text = "HAVUZ PROBLEMİ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMuslukSayisi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDugumSayisiOnayla;
    }
}

